function popUp__open(button__id, popUpClass) {
    $(button__id).click(function() {
        $(popUpClass).css('top', 0);
    })
};

function popUp__close(button__id, popUpClass) {
    $(button__id).click(function() {
        $(popUpClass).css('top', 1160);
    })
};

function change__popUp__open(button__id, popUpClasstToshow, popUpClasstToclose) {
    $(button__id).click(function() {
        $(popUpClasstToshow).css('top', 0);
        setTimeout(function(){
        	$(popUpClasstToclose).css('top', 1160);
        }, 1000)
      		
	});
};


function sideBar__open(button__id, popUpClass) {
    $(button__id).click(function() {
        $(popUpClass).css('right', 0);
    })
};

function sideBar__close(button__id, popUpClass) {
    $(button__id).click(function() {
        $(popUpClass).css('right', -600);
    })
};

function img__click(img__class){
	$(img__class).click(function() { 
	    let img = $(this); 
	    let src = img.attr('src'); 
	    $(".swiper-wrapper").append('<div class="popup">' + 
	    	'<div class="popup_bg"></div>' +  
	    	'<img src="' + src + '" class="popup_img" />' + 
	    	'</div>');
	    $(".popup").fadeIn(800); 
	    $(".popup_bg").click(function() {     
	        $(".popup").fadeOut(800); 
	        setTimeout(function() { 
	            $(".popup").remove(); 
	        }, 800);
	    });
	});
}

window.onload = function() {
    //initialize swiper when document ready
    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        speed: 1000,
        direction: 'horizontal',
        loop: true,
        effect: 'fade',
        spaceBetween: 600,
    })
    $('.go__to__first').on('click',function(){ 
    	mySwiper.slideTo(2, 100);
    });
};

$(document).ready(function() {
    // $().click(function() {
    //     mySwiper.slideTo(0, 400, true);
    // });
	img__click('.image__forclick');
    popUp__open('#ledendary7__button', '.ledendary7__popUp');
    popUp__close('#close_popUp__legendary', '.ledendary7__popUp');
    popUp__open('#fly__popUp', '.sputnik1__popUp1');
    popUp__close('#close_popUp1__sputnik1', '.sputnik1__popUp1');
    popUp__open('#popUp2__video__button', '.sputnik1__popUp2');
    popUp__close('#close_popUp2__sputnik1', '.sputnik1__popUp2');
    popUp__open('#popUp2__signal__button', '.sputnik1__popUp3');
    popUp__close('#close_popUp3__sputnik1', '.sputnik1__popUp3');
    popUp__open('#sputnik2__popUp2__open', '.sputnik2__popUp2');
    popUp__close('#close_popUp2__sputnik2', '.sputnik2__popUp2');
    change__popUp__open('#popUp2__signal__button2','.sputnik1__popUp3', '.sputnik1__popUp2')
});