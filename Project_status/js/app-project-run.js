(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var app = new Vue({
    el: '#app',
    data: function () {
        return {
		}
    },
	computed: {},
	methods: {}
});

},{}],2:[function(require,module,exports){
function popUp__open(button__id, popUpClass) {
    $(button__id).click(function() {
        $(popUpClass).css('top', 0);
    })
};

function popUp__close(button__id, popUpClass) {
    $(button__id).click(function() {
        $(popUpClass).css('top', -1080);
    })
};

function sideBar__open(button__id, popUpClass) {
    $(button__id).click(function() {
        $(popUpClass).css('right', 0);
    })
};

function sideBar__close(button__id, popUpClass) {
    $(button__id).click(function() {
        $(popUpClass).css('right', -600);
    })
};

function img__click(img__class){
	$(img__class).click(function() { 
	    let img = $(this); 
	    let src = img.attr('src'); 
	    $(".swiper-wrapper").append('<div class="popup">' + '<div class="popup_bg"></div>' +  '<img src="' + src + '" class="popup_img" />' + '</div>');
	    $(".popup").fadeIn(800); 
	    $(".popup_bg").click(function() {     
	        $(".popup").fadeOut(800); 
	        setTimeout(function() { 
	            $(".popup").remove(); 
	        }, 800);
	    });
	});
}


window.onload = function() {
    //initialize swiper when document ready
    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        speed: 1000,
        direction: 'horizontal',
        loop: true,
        effect: 'fade',
        spaceBetween: 600,
    })
};

$(document).ready(function() {
    $('.go__to__first').click(function() {
        mySwiper.slideTo(0, 400, true);
    });
	img__click('.image__forclick');
    popUp__open('#ledendary7__button', '.ledendary7__popUp');
    popUp__close('#close_popUp__legendary', '.ledendary7__popUp');
    popUp__open('#fly__popUp', '.sputnik1__popUp1');
    popUp__close('#close_popUp1__sputnik1', '.sputnik1__popUp1');
    popUp__open('#popUp2__video__button', '.sputnik1__popUp2');
    popUp__close('#close_popUp2__sputnik1', '.sputnik1__popUp2');
    popUp__open('#popUp2__signal__button', '.sputnik1__popUp3');
    popUp__close('#close_popUp3__sputnik1', '.sputnik1__popUp3');
});
},{}]},{},[2,1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJwdWJsaWMvYXBwLXByb2plY3QuanMiLCJwdWJsaWMvYmxvY2tzL2pzL2NvbW1vbi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJ2YXIgYXBwID0gbmV3IFZ1ZSh7XHJcbiAgICBlbDogJyNhcHAnLFxyXG4gICAgZGF0YTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcblx0XHR9XHJcbiAgICB9LFxyXG5cdGNvbXB1dGVkOiB7fSxcclxuXHRtZXRob2RzOiB7fVxyXG59KTtcclxuIiwiZnVuY3Rpb24gcG9wVXBfX29wZW4oYnV0dG9uX19pZCwgcG9wVXBDbGFzcykge1xyXG4gICAgJChidXR0b25fX2lkKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICAkKHBvcFVwQ2xhc3MpLmNzcygndG9wJywgMCk7XHJcbiAgICB9KVxyXG59O1xyXG5cclxuZnVuY3Rpb24gcG9wVXBfX2Nsb3NlKGJ1dHRvbl9faWQsIHBvcFVwQ2xhc3MpIHtcclxuICAgICQoYnV0dG9uX19pZCkuY2xpY2soZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgJChwb3BVcENsYXNzKS5jc3MoJ3RvcCcsIC0xMDgwKTtcclxuICAgIH0pXHJcbn07XHJcblxyXG5mdW5jdGlvbiBzaWRlQmFyX19vcGVuKGJ1dHRvbl9faWQsIHBvcFVwQ2xhc3MpIHtcclxuICAgICQoYnV0dG9uX19pZCkuY2xpY2soZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgJChwb3BVcENsYXNzKS5jc3MoJ3JpZ2h0JywgMCk7XHJcbiAgICB9KVxyXG59O1xyXG5cclxuZnVuY3Rpb24gc2lkZUJhcl9fY2xvc2UoYnV0dG9uX19pZCwgcG9wVXBDbGFzcykge1xyXG4gICAgJChidXR0b25fX2lkKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICAkKHBvcFVwQ2xhc3MpLmNzcygncmlnaHQnLCAtNjAwKTtcclxuICAgIH0pXHJcbn07XHJcblxyXG5mdW5jdGlvbiBpbWdfX2NsaWNrKGltZ19fY2xhc3Mpe1xyXG5cdCQoaW1nX19jbGFzcykuY2xpY2soZnVuY3Rpb24oKSB7IFxyXG5cdCAgICBsZXQgaW1nID0gJCh0aGlzKTsgXHJcblx0ICAgIGxldCBzcmMgPSBpbWcuYXR0cignc3JjJyk7IFxyXG5cdCAgICAkKFwiLnN3aXBlci13cmFwcGVyXCIpLmFwcGVuZCgnPGRpdiBjbGFzcz1cInBvcHVwXCI+JyArICc8ZGl2IGNsYXNzPVwicG9wdXBfYmdcIj48L2Rpdj4nICsgICc8aW1nIHNyYz1cIicgKyBzcmMgKyAnXCIgY2xhc3M9XCJwb3B1cF9pbWdcIiAvPicgKyAnPC9kaXY+Jyk7XHJcblx0ICAgICQoXCIucG9wdXBcIikuZmFkZUluKDgwMCk7IFxyXG5cdCAgICAkKFwiLnBvcHVwX2JnXCIpLmNsaWNrKGZ1bmN0aW9uKCkgeyAgICAgXHJcblx0ICAgICAgICAkKFwiLnBvcHVwXCIpLmZhZGVPdXQoODAwKTsgXHJcblx0ICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkgeyBcclxuXHQgICAgICAgICAgICAkKFwiLnBvcHVwXCIpLnJlbW92ZSgpOyBcclxuXHQgICAgICAgIH0sIDgwMCk7XHJcblx0ICAgIH0pO1xyXG5cdH0pO1xyXG59XHJcblxyXG5cclxud2luZG93Lm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgLy9pbml0aWFsaXplIHN3aXBlciB3aGVuIGRvY3VtZW50IHJlYWR5XHJcbiAgICB2YXIgbXlTd2lwZXIgPSBuZXcgU3dpcGVyKCcuc3dpcGVyLWNvbnRhaW5lcicsIHtcclxuICAgICAgICAvLyBPcHRpb25hbCBwYXJhbWV0ZXJzXHJcbiAgICAgICAgc3BlZWQ6IDEwMDAsXHJcbiAgICAgICAgZGlyZWN0aW9uOiAnaG9yaXpvbnRhbCcsXHJcbiAgICAgICAgbG9vcDogdHJ1ZSxcclxuICAgICAgICBlZmZlY3Q6ICdmYWRlJyxcclxuICAgICAgICBzcGFjZUJldHdlZW46IDYwMCxcclxuICAgIH0pXHJcbn07XHJcblxyXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcclxuICAgICQoJy5nb19fdG9fX2ZpcnN0JykuY2xpY2soZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgbXlTd2lwZXIuc2xpZGVUbygwLCA0MDAsIHRydWUpO1xyXG4gICAgfSk7XHJcblx0aW1nX19jbGljaygnLmltYWdlX19mb3JjbGljaycpO1xyXG4gICAgcG9wVXBfX29wZW4oJyNsZWRlbmRhcnk3X19idXR0b24nLCAnLmxlZGVuZGFyeTdfX3BvcFVwJyk7XHJcbiAgICBwb3BVcF9fY2xvc2UoJyNjbG9zZV9wb3BVcF9fbGVnZW5kYXJ5JywgJy5sZWRlbmRhcnk3X19wb3BVcCcpO1xyXG4gICAgcG9wVXBfX29wZW4oJyNmbHlfX3BvcFVwJywgJy5zcHV0bmlrMV9fcG9wVXAxJyk7XHJcbiAgICBwb3BVcF9fY2xvc2UoJyNjbG9zZV9wb3BVcDFfX3NwdXRuaWsxJywgJy5zcHV0bmlrMV9fcG9wVXAxJyk7XHJcbiAgICBwb3BVcF9fb3BlbignI3BvcFVwMl9fdmlkZW9fX2J1dHRvbicsICcuc3B1dG5pazFfX3BvcFVwMicpO1xyXG4gICAgcG9wVXBfX2Nsb3NlKCcjY2xvc2VfcG9wVXAyX19zcHV0bmlrMScsICcuc3B1dG5pazFfX3BvcFVwMicpO1xyXG4gICAgcG9wVXBfX29wZW4oJyNwb3BVcDJfX3NpZ25hbF9fYnV0dG9uJywgJy5zcHV0bmlrMV9fcG9wVXAzJyk7XHJcbiAgICBwb3BVcF9fY2xvc2UoJyNjbG9zZV9wb3BVcDNfX3NwdXRuaWsxJywgJy5zcHV0bmlrMV9fcG9wVXAzJyk7XHJcbn0pOyJdfQ==
