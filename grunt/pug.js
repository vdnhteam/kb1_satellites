module.exports = {
	pug: {
		options: {
			data: {
				debug: false
			}
		},
		files: {
			'<%=project.dist %>/index.html': ['<%=project.app %>/sources/index.pug']
		}
	}
};
